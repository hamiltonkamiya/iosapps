//
//  Model.swift
//  SwifUIDemo
//
//  Created by user189204 on 1/30/21.
//

import Foundation

struct Weather: Identifiable {
    var id = UUID()
    var image: String
    var temp: Int
    var city: String
}

let modelData: [Weather] =
    [Weather(image:"cloud.rain", temp: 21, city: "Amsterdam"),
    Weather(image: "cloud.sun.rain", temp: 18, city: "London"),
    Weather(image: "sun.max", temp: 25, city: "Paris"),
    Weather(image: "cloud.sun", temp: 23, city: "Tokyo"),
    Weather(image: "cloud.sun", temp: 23, city: "Sao Bernnardo"),
    Weather(image: "cloud.sun", temp: 23, city: "San Francisco")]
