//
//  ContentView.swift
//  SwifUIDemo
//
//  Created by user189204 on 1/29/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView() {
            List(modelData) { weather in
                HStack {
                    Image(systemName: weather.image)
                            .frame(width: 50, height: 10, alignment: .leading)
                    Text("\(weather.temp)º")
                            .frame(width: 50, height: 10, alignment: .leading)
                    VStack {
                        Text(weather.city)
                    }
                }.font(.title)

            }
            .navigationBarTitle(Text("World Weather US$0.99 "))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
