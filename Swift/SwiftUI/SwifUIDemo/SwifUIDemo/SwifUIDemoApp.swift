//
//  SwifUIDemoApp.swift
//  SwifUIDemo
//
//  Created by user189204 on 1/29/21.
//

import SwiftUI

@main
struct SwifUIDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
