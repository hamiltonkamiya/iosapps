//
//  ViewController.swift
//  MVVM-Swift5-iOS-TableView
//
//  Created by user189204 on 1/12/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var usernameTextField:UITextField!
    @IBOutlet weak var passwordTextField:UITextField!
    let viewModelLogin = ViewModelLogin()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModelLogin.delegate = self
        
    }
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        viewModelLogin.itemsDownload()
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        // ...
        // after login is done, maybe put this in the login web service completion block

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "MainTabBarController")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    

}

extension ViewController: ViewModelDelegate {
    
    func downloadedItems() {
        /*
        let alert = UIAlertController(title: "MVVM Swift 5", message: "sample message here by Hamilton", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        alert.addAction(OKAction)
        present(alert, animated: true, completion: nil)
        */
        if LoginManager.sharedInstance.isLoggedIn == true {
            
        }
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let  nextViewController = storyBoard.instantiateViewController(withIdentifier: "loggedTableView") as! TableViewController
        //let  nextViewController = storyBoard.instantiateViewController(withIdentifier: "loggedTabBar") as! TabBarController
        let navig = UINavigationController(rootViewController: nextViewController)
        //nextViewController.modalPresentationStyle = .fullScreen
        //self.present(nextViewController, animated: true, completion: nil)
        
        //navigationController?.pushViewController(nextViewController, animated: true)
        navigationController?.pushViewController(nextViewController, animated: true)
        /*
        if let tabBarController = storyboard?.instantiateViewController(withIdentifier: "loggedTabBar") as? TabBarController {
            present(tabBarController, animated: true, completion: nil)
        }
        */
        
    }
    
}
