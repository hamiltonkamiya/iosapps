//
//  File.swift
//  MVVM-Swift5-iOS-TableView
//
//  Created by user189204 on 1/12/21.
//

import Foundation

struct model: Codable {
    let id:Int?
    let name:String?
    let phone:String?
    let price:String?
}
