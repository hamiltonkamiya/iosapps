//
//  TabBarOneDetailViewController.swift
//  MVVM-Swift5-iOS-TableView
//
//  Created by user189204 on 1/26/21.
//

import UIKit

class TabBarOneDetailViewController: UIViewController {

    
    @IBOutlet weak var usernameTextField: UITextField!
    
    //let button = UIButton(frame: CGRect(x: 10, y: 500, width: 200, height: 34))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        /*
        button.backgroundColor = UIColor.gray
        view.addSubview(button)
        button.addTarget(self, action: #selector(tappedButton), for: .touchUpInside)
        */
        
        let backButton = UIBarButtonItem(title: "Back",
                                                    style: UIBarButtonItem.Style.plain, target: self, action: #selector(btnBackHome))
        navigationItem.leftBarButtonItem = backButton
                
        //let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        //let play = UIBarButtonItem(title: "Play", style: .plain, target: self, action: #selector(playTapped))
                
        //navigationItem.rightBarButtonItems = [add, play]
        
    }
    
    @objc func btnBackHome(_ sender: UIBarButtonItem) -> Void {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isModal:Bool = ((presentingViewController?.isModalInPresentation) != nil)
        if isModal == true {
            dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func tappedButton() {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
