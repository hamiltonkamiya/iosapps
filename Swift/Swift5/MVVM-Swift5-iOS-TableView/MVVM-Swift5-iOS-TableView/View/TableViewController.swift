//
//  TableViewController.swift
//  MVVM-Swift5-iOS-TableView
//
//  Created by user189204 on 1/13/21.
//

import UIKit
import RxSwift
import RxCocoa

class TableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let disposeBag =  DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.red
        /*
        let service = Products()
        service.connectRxDatabase()
            .asObservable()
            .map { $0 }
            .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: UITableViewCell.self)) { (row, element, cell) in
                cell.textLabel?.text = "\(element.name ?? "text") @ row \(row)"
                //cell.labelUsername.text = "\(element.name ?? "text") @ row \(row)"
            }
            .disposed(by: disposeBag)
        */
        let service = Products()
        service.connectRxDatabase()
            .asObservable()
            .map { $0 }
            .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: TableViewCell.self)) { (row, element, cell) in
                cell.labelUsername.text = "\(element.name ?? "text") @ row \(row)"
            }
            .disposed(by: disposeBag)
            
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationItem.title = "RxSwift for iOS App"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Voltar", style: .plain, target: self, action: #selector(btnBackHome))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(btnAddItem))
        
    }
    
    @objc func btnBackHome(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func btnAddItem(){
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

struct ViewModel {
    let input: Input
    let output: Output

    struct Input {
        let search: PublishRelay<String>
    }

    struct Output {
        let targets: Driver<[model]>
    }

    init(targets: [model] = []) {

        let targetSubject = PublishRelay<[model]>()
        let searchSubject = PublishRelay<String>()

        let targets = searchSubject
            .startWith("")
            .distinctUntilChanged()
            .withLatestFrom(targetSubject.startWith(targets).asObservable()) { (search, targets) in (search, targets)}
            .map({ (search, targets) -> [model] in
                return targets.filter({ $0.name!.hasPrefix(search) || $0.name!.contains(" \(search)") })
            })
            .asDriver(onErrorJustReturn: [])

        self.input = Input(search: searchSubject)
        self.output = Output(targets: targets)
    }
}
