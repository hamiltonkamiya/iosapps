//
//  CollectionViewCell.swift
//  MVVM-Swift5-iOS-TableView
//
//  Created by user189204 on 1/20/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(text: String) {
        self.textLabel.text = text
    }

}
