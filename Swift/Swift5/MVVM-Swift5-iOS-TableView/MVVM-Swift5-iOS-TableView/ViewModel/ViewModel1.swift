//
//  ViewModel1.swift
//  MVVM-Swift5-iOS-TableView
//
//  Created by user189204 on 1/12/21.
//

import UIKit
import RxSwift
import RxCocoa

class ViewModel1: UIViewController {

    let disposeBag =  DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let service = Products()
        service.connectRxDatabase().subscribe({ products in
            print(products)
            //display UIAlertController
            
            //display with RxCocoa
            
        }).disposed(by: disposeBag)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
