//
//  ViewModelDelegate.swift
//  MVVM-Swift5-iOS-TableView
//
//  Created by user189204 on 1/12/21.
//

import UIKit

protocol ViewModelDelegate: class {
    func downloadedItems()
}

class ViewModelLogin {
    
    var delegate: ViewModelDelegate?
    
    func itemsDownload(){
        delegate?.downloadedItems()
    }
}
