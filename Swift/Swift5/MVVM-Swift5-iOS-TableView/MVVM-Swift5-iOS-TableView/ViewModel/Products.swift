//
//  Products.swift
//  MVVM-Swift5-iOS-TableView
//
//  Created by user189204 on 1/12/21.
//

import Foundation
import RxSwift

class Products {
    
    func connectRxDatabase()-> Observable<[model]> {
        return Observable.create{ (observer)-> Disposable in
            let url = URL(string: "https://javarestjson.herokuapp.com/api/produtos")
            let request = URLRequest(url: url!)
            let task = URLSession.shared.dataTask(with: request){
                data, response, error in
                guard let data = data else {
                    observer.onError(NSError(domain: "", code: -1, userInfo: nil))
                    return
                }
                
                do {
                    let products = try JSONDecoder().decode([model].self, from: data)
                    observer.onNext(products)
                } catch {
                    observer.onError(error)
                }
                
                
            }
            task.resume()
            return Disposables.create {}
        }
    }
}
