//
//  Webservice.swift
//  0iOS-Swift-RESTful
//
//  Created by macOS on 22/07/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class Webservice: NSObject, NSURLConnectionDelegate,NSURLConnectionDataDelegate, UIAlertViewDelegate {
    
    var data = NSMutableData()
    private var conn:NSURLConnection?
    
    func connect(query: String) {
        let url =  NSURL(fileURLWithPath: query)
        let request = NSURLRequest(URL: url)
        conn = NSURLConnection(request: request, delegate: self, startImmediately: true)
    }
    
    func connection(didReceiveResponse: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        self.data.length = 0
    }
    
    func connection(connection: NSURLConnection, didReceiveData conData: NSData) {
        self.data.appendData(conData)
    }
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        print(error)
    }
    func connectionDidFinishLoading(connection: NSURLConnection) {
        //println(self.data)
        do {
            let stringToAlert:String = (String(data: self.data, encoding: NSUTF8StringEncoding)!)
            print(stringToAlert)
            let showAlertForMe = UIAlertView(title: "iOS REST", message: stringToAlert, delegate: nil, cancelButtonTitle: "Cancelar")
            showAlertForMe.show()
        } catch {
            print(error)
        }
        
        /*
        
        */
    }
    
    
    deinit {
        //println("deiniting")
    }
}