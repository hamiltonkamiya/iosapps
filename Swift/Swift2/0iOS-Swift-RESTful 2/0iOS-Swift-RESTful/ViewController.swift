//
//  ViewController.swift
//  0iOS-Swift-RESTful
//
//  Created by macOS on 18/07/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mainView: UIView?
    var webservice = Webservice()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView = UIView()
        webservice.connect("https://jsonplaceholder.typicode.com/todos/1")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

