//
//  ViewController.swift
//  MVC-Swift-Lavoropiu
//
//  Created by macOS on 15/11/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func loadMySQL(name:String, completion:(String)-> Void){
        let nameDisplay:String = name
        completion(nameDisplay)
    }

}

