//
//  InterfaceController.swift
//  MVC-Swift2-WatchOS WatchKit Extension
//
//  Created by macOS on 04/03/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    @IBOutlet var tableView: WKInterfaceTable!
    
    let tableData = ["user 01", "user 02", "user 03"]
    
    //https://watchcoder.com/2019/07/29/how-to-display-a-list-of-items-on-an-apple-watch-watchos-swift-tutorial/
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        loadTableData()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    private func loadTableData(){
        tableView.setNumberOfRows(tableData.count, withRowType: "RowController")
        
        for(index, rowModel) in tableData.enumerate() {
            if let rowController = tableView.rowControllerAtIndex(index) as? rowController{
                rowController.rowLabel.setText(rowModel)
            }
        }
    }

}
