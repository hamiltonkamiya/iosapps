//
//  ViewController.swift
//  SimpleApp
//
//  Created by macOS on 24/07/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var simpleView: UIView!
    
    @IBOutlet weak var textLabel: UILabel!
    var textHere:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let url = NSURL(string: "http://heyhttp.org/me.json")
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, ErrorType) -> Void in
            
            if let urlContent = data {
                
                do {
                    
                    let jsonResult = try NSJSONSerialization.JSONObjectWithData(urlContent, options: NSJSONReadingOptions.MutableContainers)
                    
                    print(jsonResult)
                    
                    let text = jsonResult as? String
                    self.textHere = text
                    dispatch_async(dispatch_get_main_queue()) {
                        self.textLabel.text = self.textHere
                    }
                    
                } catch {
                    
                    print("JSON serialization failed")
                    
                }
                
            }
            
        }
        
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

