//
//  ViewController.swift
//  0iOS-Swift-Xib
//
//  Created by macOS on 18/07/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController, NSURLConnectionDataDelegate {
    
    var data: NSMutableData = NSMutableData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callingWebservice("http://pizzaria2.000webhostapp.com/android_connect/get_all_products.php")
        //view.backgroundColor = UIColor.init(white: 1.0, alpha: 0.0)
        let button = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 50))
        button.backgroundColor = .yellowColor()
        button.setTitle("Test Button", forState: .Normal)
        button.addTarget(self, action: Selector("buttonAction:"), forControlEvents: .TouchUpInside) //Swift 2.2
        //button.addTarget(self, action: #selector(buttonAction), forControlEvents: .TouchUpInside) // after Swift 2.2
        //https: //learnappmaking.com/target-action-swift/
        
        self.view.addSubview(button)
        
        /*
        let myFirstAlert = UIAlertView(title: "iOS RESTful", message: "App Started", delegate: nil, cancelButtonTitle: "cancelar")
        myFirstAlert.show()
        */
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func buttonAction(sender: UIButton!) {
        
        if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
        
        }
        
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func callingWebservice(myURL: String){
        let urlPath = myURL
        let url = NSURL(string: urlPath)
        let request: NSURLRequest = NSURLRequest(URL: url!)
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
        connection.start()
    }
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse)
    {
        self.data = NSMutableData()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        self.data.appendData(data)
    }
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        let myTextGoesHere  = NSString(data: self.data, encoding: NSUTF8StringEncoding) as! String
        
        let myAlert = UIAlertView(title: "iOS RESTful", message: myTextGoesHere, delegate: nil, cancelButtonTitle: "cancelar")
        myAlert.show()
        
    }

}

/*
extension Selector {
static let fireworks = #selector(Effects.fireworks)
static let onButtonTapped = #selector(ViewController.onButtonTapped(_:))
}
*/