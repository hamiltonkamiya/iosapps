//
//  mainView.swift
//  0iOS-Swift-Xib
//
//  Created by macOS on 18/07/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

@IBDesignable
class mainView: UIView {

    @IBOutlet weak var contentView:UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        //let myBundle = NSBundle.mainBundle().loadNibNamed("mainView", owner: self, options: nil)
    }

}