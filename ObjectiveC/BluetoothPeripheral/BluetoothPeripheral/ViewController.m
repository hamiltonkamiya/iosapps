//
//  ViewController.m
//  BluetoothPeripheral
//
//  Created by macOS on 14/03/20.
//  Copyright © 2020 macOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController() {
    CBPeripheralManager *_pManager;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _peripheral = [[CBPeripheralManager alloc] init];
    _data = [[NSMutableData alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOff:
            //dispatch_async(dispatch_get_main_queue(), <#^(void)block#>);
            
            break;
        case CBPeripheralManagerStatePoweredOn:
            
            break;
        case CBPeripheralManagerStateResetting:
            break;
        case CBPeripheralManagerStateUnauthorized:
            break;
        case CBPeripheralManagerStateUnknown:
            break;
            case CBPeripheralManagerStateUnsupported:
            break;
        default:
            break;
    }
}
@end
