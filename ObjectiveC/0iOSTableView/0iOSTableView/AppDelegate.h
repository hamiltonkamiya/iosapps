//
//  AppDelegate.h
//  0iOSTableView
//
//  Created by macOS on 11/06/19.
//  Copyright © 2019 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

