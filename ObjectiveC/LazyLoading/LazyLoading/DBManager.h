//
//  DBManager.h
//  LazyLoading
//
//  Created by macOS on 18/10/18.
//  Copyright © 2018 macOS. All rights reserved.
//  www.tutorialspoint.com/ios/ios_sqlite_database.htm
// www.appcoda.com/ios-programming-tutorial-create-a-simple-table-view-pp/
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject {
    NSString *databasePath;
}

+(DBManager*)getSharedInstance;
-(BOOL)createDB;
-(BOOL) saveData:(NSString*)registerNumber name:(NSString*)name
      department:(NSString*)department year:(NSString*)year;
-(NSArray*) findByRegisterNumber:(NSString*)registerNumber;

@end