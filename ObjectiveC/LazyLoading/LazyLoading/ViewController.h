//
//  ViewController.h
//  LazyLoading
//
//  Created by macOS on 12/09/18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property (readonly, nonatomic) NSArray *tableData;

@end