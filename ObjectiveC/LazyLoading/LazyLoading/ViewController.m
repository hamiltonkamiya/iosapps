//
//  ViewController.m
//  LazyLoading
//
//  Created by macOS on 12/09/18.
//  Copyright © 2018 macOS. All rights reserved.
//  Hamilton Kamiya
//
// www.appcoda.com/ios-programming-tutorial-create-a-simple-table-view-pp/
//

#import "ViewController.h"

@interface ViewController ()
{
    //NSArray *tableData;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // Initialize table data
    // www.appcoda.com/ios-programming-tutorial-create-a-simple-table-view-app/
    
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    
    _tableData = [NSArray arrayWithObjects:@"username 01", @"username 02", @"username 03", @"username 04", @"username 05", @"username 06", @"username 07", @"username 08", @"username 09", @"username 10", @"Churrasco linguica toscana", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [_tableData objectAtIndex:indexPath.row];
    return cell;
}

@end
