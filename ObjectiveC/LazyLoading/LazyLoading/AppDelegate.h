//
//  AppDelegate.h
//  LazyLoading
//
//  Created by macOS on 12/09/18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

// Forward declaration (Used when class will be defined /imported in future)
@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITableViewController *tableViewController;

@end
