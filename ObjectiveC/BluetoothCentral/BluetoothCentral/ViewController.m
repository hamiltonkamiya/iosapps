//
//  ViewController.m
//  BluetoothCentral
//
//  Created by macOS on 16/03/20.
//  Copyright © 2020 macOS. All rights reserved.
//

#import "ViewController.h"

#define BLE_SERVICE_UUID @"E20A39F4-73F5-4BC4-A12F-17D1AD07A961"
#define BLE_CHARACTERISTIC_UUID @"08590F7E-B05-467E-8757-72F6FAEB13D4"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UITextView   *textview;
@property (strong, nonatomic) CBCentralManager      *centralManager;
@property (strong, nonatomic) CBPeripheral          *discoveredPeripheral;
@property (strong, nonatomic) NSMutableData         *data;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //https://developer.apple.com/library/archive/samplecode/BTLE_Transfer/Introduction/Intro.html
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    _data = [[NSMutableData alloc] init];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)centralManagerDidUpdateState:(CBCentralManager *)central {
    switch (central.state) {
        case CBCentralManagerStatePoweredOff:
            
            break;
        case CBCentralManagerStatePoweredOn:
            //[self performSelectorOnMainThread:@selector(showAlert:) withObject:alertString waitUntilDone:NO];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"my app alert"                                                        message:@"something went wrong"                                                      delegate:nil                                              cancelButtonTitle:@"close"                                              otherButtonTitles:nil];
                [alertView show];

            });
            
            return;
            break;
        case CBCentralManagerStateResetting:
            
            break;
        case CBCentralManagerStateUnauthorized:
            
            break;
        case CBCentralManagerStateUnknown:
            
            break;
        case CBCentralManagerStateUnsupported:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertViewUnsupported = [[UIAlertView alloc] initWithTitle:@"Objective C Bluetooth"                                                        message:@"BLE Unsupported"                                                      delegate:nil                                              cancelButtonTitle:@"close"                                              otherButtonTitles:nil];
                [alertViewUnsupported show];
                
            });

            break;
        default:
            break;
    }
    [self scan];
}

-(void)showAlert {
    
}

- (void)scan
{
    NSLog(@"Scanning started");
}

-(void)makeBlock {
    int result = multiplyTwoValues(2,4);
    NSLog(@"The result is %d", result);
}

void (^simleBlock)(void) = ^{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"My Alert"
                                                                   message:@"This is an alert."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    //[self presentViewController:alert animated:YES completion:nil];
};

int (^multiplyTwoValues)(int, int) =
^(int firstValue, int secondValue) {
    return firstValue * secondValue;
};


@end
