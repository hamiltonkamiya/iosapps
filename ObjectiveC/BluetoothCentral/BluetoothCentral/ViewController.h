//
//  ViewController.h
//  BluetoothCentral
//
//  Created by macOS on 16/03/20.
//  Copyright © 2020 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ViewController : UIViewController<CBCentralManagerDelegate, CBPeripheralDelegate>

-(void)showAlert;
-(void)scan;
-(void)makeBlock;

@end
